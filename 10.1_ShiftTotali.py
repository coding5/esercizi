from random import randrange
w = 5
h = 4
A = [[randrange (10) for x in range(w)] for y in range(h)] 

for i in range(h):
    for j in range(w):
        print(A[i][j], end='')
    print()



for i in range(h):
    for j in range(w-1):
        A[i][j] = A[i][j+1]

print()

for i in range(h):
    for j in range(w):
        print(A[i][j], end='')
    print()


for i in range(h):
    for j in reversed(range(1, w)):
            A[i][j] = A[i][j-1]

print()

for i in range(h):
    for j in range(w):
        print(A[i][j], end='')
    print()