from random import randrange
A = [(randrange(10)) for i in range (5)]

for i in range(len(A)):
    print(A[i], end='')

for i in range(len(A)-1):
    A[i] = A[i+1]

print()

for i in range(len(A)):
    print(A[i], end='')



for i in reversed(range(1, len(A)) ):
    A[i] = A[i-1]

print()

for i in range(len(A)):
    print(A[i], end='')