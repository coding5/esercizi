v = []
i = 0
trovati = 0

Len = int(input("Inserisci il numero di valori tra cui vuoi cercare: "))

for q in range(Len):
    v.append(int(input("Inserisci il valore della cella {}: ".format(q))))

X = int(input('Inserisci il valore da cercare: '))

v.sort()

for i in range(Len) :
    if v[i] == X:
        trovati += 1

if trovati == 1:
    print("Trovata una corrispondenza")
else:
    print("Trovate {} corrispondenze" .format(trovati))