from random import randrange

class Rectangle:    #Definisco una classe che si riferisce a un rettangolo che ha una base e un'altezza

    #self è un argomento che si riferisce all’istanza
    #__init__ è un metodo chiamato quando viene creata un'istanza
    def __init__(self, base, height):   
        #istanza.attributo = valore
        self.base = base    #Attributo di istanza che riceve la base
        self.height = height    #Attributo di istanza che riceve l'altezza

    def Area(self):
        return self.base * self.height

    def Perimeter(self):
        return (self.base + self.height) * 2

#Lista di 10 rettangoli casuali che si riferiscono alla classe rettangoli perchè ... = [Rectangle ...
rects = [Rectangle(randrange(100), randrange(100)) for x in range(10)]    

for Rettangolo in rects:    #Per ogni elemento della lista rects, calcolo Area e perimetro
    print("Rettangolo con base {} e altezza {}".format(Rettangolo.base, Rettangolo.height))
    print("    Area: ", Rettangolo.Area())
    print("    Perimetro: ", Rettangolo.Perimeter())