V = []
N = 10

for i in range(N):
    V.append(int(input()))

V.sort()

while i < len(V)-1:    #-1 per non uscire dal range l'ultima volta con l'i + 1
    if V[i] == V[i+1]:
        V.remove(V[i])
        N -= 1   #Quando rimuovo un elemento, la lista mi si accorcia, quindi devo usare questo per evitare di uscire dal range dell'indice
        if i > 0:    #Altrimenti diventa negativo e fa casini
            i -= 1
    else:
        i += 1

print ("\n-------\n")
for i in range(N):
    print(V[i])
