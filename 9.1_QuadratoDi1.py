w = 8
h = 5

A = [[0 for x in range(w)] for y in range(h)]

for i in range(h):
    for j in range(w):
        A[i][j] = 0

for i in range(w):
    A[0][i] = 1
    A[h-1][i] = 1

for i in range(h):
    A[i][0] = 1
    A[i][w-1] = 1

for i in range(h):
    for j in range(w):
        print(A[i][j], end='')
    print('')