#Questa è l'implementazione strana, lenta, complicata e insensata dell'algoritmo che conta quante volte un numero
#si ripete in un array, però funziona, quella normale è (o almeno era quando ho scritto questo):
#7.1_ContaRipetizioniNormale.py

v = []
i = 0
trovato = False
trovati = 0

for q in range(10):
    v.append(int(input("Inserisci il valore della cella {}: ".format(q))))

X = int(input('Inserisci un numero da cercare'))

v.sort()
Max = len(v)

while i < Max and not trovato:
    if v[i] == X:
        trovato = True
        trovati = 0 # =0 e non a 1 perchè quello lo conto la prima ripetizione delprimo ciclo
        j = i - 1 #j parte da quello prima di i e va indietro, i va avanti

        while i < Max:
            if v[i] == X:        
                trovati += 1
            i += 1

        while j >= 0:
            if v[j] == X:
                trovati += 1
            j -= 1

        break

    i += 1

print(trovati)