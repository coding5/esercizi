#Date tre numeri che dovrebbero essere i lati di un triangolo (ma non è detto che lo siano)
#calcolare se il triangolo è isoscele, scaleno rettangolo ecc...
#e se avanza del tempo anche angoli e area (dopo svariati mesi non sembra ne sia avanzato)

from math import sqrt as r

A = int(input("Inserisci il primo lato: "))
B = int(input("Inserisci il secondo lato: "))
C = int(input("Inserisci il terzo lato: "))

Max = "A"
MaxVal = A
Scaleno = True

if B > MaxVal:
    Max = "B"
    MaxVal = B
if C > MaxVal:
    Max = "C"
    MaxVal = C

if (Max=="A" and A > B+C or Max=="B" and B > A+C or Max=="C" and C > A+B) or (A <= 0 or B <= 0 or C <= 0):
    print("I lati inseriti non possono formare un triangolo")

else:
    if A == B and B == C:
        print("Il triangolo è equilatero")
        Scaleno = False

    elif A == B or B == C or A == C:
        print("Il triangolo è isoscele")
        Scaleno = False

    if Max == "A":
        if A == r(B*B+C*C):
            print("Il triangolo è rettangolo")
            Scaleno = False
    elif Max == "B":
        if B == r(A*A+C*C):
            print("Il triangolo è rettangolo")
            Scaleno = False
    elif Max == "C":
        if C == r(A*A+B*B):
            print("Il triangolo è rettangolo")
            Scaleno = False

    if Scaleno:
        print("Il triangolo è scaleno")
    