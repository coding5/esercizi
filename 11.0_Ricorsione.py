def F1(n):
    if(n > 0):
        F1(n-1)
        print(n)

def F2(n):
    if(n > 0):
        print(n)
        F1(n-1)

def P(b, e):
    if(e > 0):
        b = b*P(b, e-1)
    else:
        return 1
    return b


print(P(6, 4))