import time
import math

Err = True    #Non so fare i do while quindi faccio in modo che la condizione sia sempre vera la prima volta
while Err == True:    
    try:    #Provo a leggere la variabile
        x1 = float(input("Inserisci la coordinata X del primo punto: "))
    except ValueError:
        print ("La coordinata deve essere un numero intero o decimale")
    else:    #Se ci riesco smetto di provare
        Err = False

Err = True
while Err == True:
    try:
        y1 = float(input("Inserisci la coordinata Y del primo punto: "))
    except ValueError:
        print ("La coordinata deve essere un numero intero o decimale")
    else:
        Err = False

Err = True
while Err == True:
    try:
        x2 = float(input("Inserisci la coordinata X del secondo punto: "))
    except ValueError:
        print ("La coordinata deve essere un numero intero o decimale")
    else:
        Err = False

Err = True
while Err == True:
    try:
        y2 = float(input("Inserisci la coordinata Y del secondo punto: "))
    except ValueError:
        print ("La coordinata deve essere un numero intero o decimale")
    else:
        Err = False



#----Calcolo i delta----#
DeltaX = x1 - x2
DeltaY = y1 - y2

#----Calcolo il punto medio e la lunghezza----#
PuntoMedioX =  (x1+x2)/ 2
PuntoMedioY = (y1+y2) / 2
Lunghezza = math.sqrt(DeltaX*DeltaX + DeltaY*DeltaY)

#----Calcolo l'angolo formato con l'asse x----#

#Traslo il segmento in modo che il primo punto coincida con l'origine
x2 = x2 - x1 
y2 = y2 - y1
x1 = 0
y1 = 0

if x2 != 0:
    Alfa = math.atan(DeltaY/DeltaX)  #Uso la formula magica per calcolarmelo sapendo la lunghezza delle proiezioni
    Alfa = Alfa*360/math.tau  #Converto i radianti in gradi

    if x2 < 0:
        Alfa = Alfa + 180

else:  #Non posso dividere per 0, se la componente x è 0 l'angolo è 90° o -90°
    if y2 > 0:
        Alfa = 90
    else:
        Alfa = 270

print("La lunghezza del segmento è: ", Lunghezza)
print("Le coordinate del punto medio del segmento sono: ", PuntoMedioX, PuntoMedioY)
print("Traslato il segmento in modo che x1 = 0 e y1 = 0, il segmento forma con la direzione positiva dell'asse delle X un angolo di", Alfa)

time.sleep(7.3)