N = 0    #Mi indicherà quante volte ho eseguito il programma

with open("test.txt", "a") as f:    #Apro il file in scrittura
    f.write("x")    #Ci scrivo dentro una x ogni volta che lo apro

with open("test.txt", "r") as f:   #Lo riapro in lettura (simile a f.open ma meglio)
    while f.read(1) == "x":    #Ogni volta che leggo x, aumento il numero di x che ho letto (leggo un solo carattere alla volta)
        N += 1

print("Programma Eseguito {} volte".format(N))
