import os

while True:
    os.system('cls')

    print("Programmi: ")
    print("0) HelloWorld")
    print("1) Basi")
    print("2) Segmenti")
    print("3) I-O su file")
    print("4) Oggetti")     #Non so se questo funziona o no
    print("5) Persone")
    print("6) SortECancellaDoppioni")   #Non mi ricordo come si chiama questo algoritmo
    print("7) BubbleSortECancellaDoppioni")
    print("8) RicercaClassica")     #Non mi ricordo come si chiama questo algoritmo
    print("9) RicercaAvanzata")     #Non mi ricordo come si chiama questo algoritmo
    print("10) ContaRipetizioniStrano")
    print("11) ContaRipetizioniNormale")
    print("12) TriangoloDiFloyd")
    print("13) ClassificareUnTriangolo")
    print("14) EsercizioStrano")
    print("15) QuadratoDi1")
    print("16) ShiftParziali")
    print("17) ShiftTotali")
    print("18) Ricorsione")


    n = int(input("Inserisci un numero: "))

    if n == 0:
        os.system('cls')
        os.system("0_HelloWorld.py")
    if n == 1:
        os.system('cls')
        os.system("1.0_Basi.py")
    if n == 2:
        os.system('cls')
        os.system("2.0_Segmenti.py")
    if n == 3:
        os.system('cls')
        os.system("3.0_I-OSu file.py")
    if n == 4:
        os.system('cls')
        os.system("4.0_Oggetti.py")
    if n == 5:
        os.system('cls')
        os.system("4.1_Oggetti2.py")
    if n == 6:
        os.system('cls')
        os.system("5.0_SortECancellaDoppioni.py")
    if n == 7:
        os.system('cls')
        os.system("5.1_BubbleSortECancellaDoppioni.py")
    if n == 8:
        os.system('cls')
        os.system("6.0_RicercaClassica.py")
    if n == 9:
        os.system('cls')
        os.system("6.1_RicercaAvanzata.py")
    if n == 10:
        os.system('cls')
        os.system("7.0_ContaRipetizioniStrano.py")
    if n == 11:
        os.system('cls')
        os.system("7.1_ContaRipetizioniNormale.py")
    if n == 12:
        os.system('cls')
        os.system("8.0_TriangoloDiFloyd.py")
    if n == 13:
        os.system('cls')
        os.system("8.1_ClassificareUnTriangolo.py")
    if n == 14:
        os.system('cls')
        os.system("9.1_QuadratoDi1.py")
    if n == 15:
        os.system('cls')
        os.system("9.2_EsercizioStrano.py")
    if n == 16:
        os.system('cls')
        os.system("10.0_ShiftParziali.py")
    if n == 17:
        os.system('cls')
        os.system("10.1_ShiftTotali.py")
    if n == 18:
        os.system('cls')
        os.system("11.0_Ricorsione.py")

    print('\n')
    os.system('pause')