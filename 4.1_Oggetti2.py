I = 0
p = ["-"]
Linee = 12 #Per ora va modificato ogni volta che aggiungo un cibo nel documento

class Persona:
    def __init__(self, nome, cognome, veg, lavoro):
        self.nome = nome
        self.cognome = cognome
        self.veg = veg
        self.lavoro = lavoro
    
    def mangia(self, attribcibo):
        CiboVegano = False    #Variabile che mi dice se il cibo inserito è vegano o no
        with open("CibiVegani.txt", "r") as Vegani:    #Non è ottimizzato dato che apro il file a prescindere da tutto
            Vegani = Vegani.readlines(Linee)
            for i in Vegani:
                if attribcibo == i:
                    CiboVegano = True
                    break

        if attribcibo == "niente" or attribcibo == "nulla" or attribcibo == "": #dovrei aggiungere anche " ", "  ", "   " ecc...
            print("{} {} oggi sta a dieta".format(self.nome, self.cognome))
        elif CiboVegano == False and self.veg == True:
            print("{} {} è vegano perchè ama la natura, quindi non può mangiare {}".format(self.nome, self.cognome, attribcibo))
        else:
            print("{} {} sta mangiando {}".format(self.nome, self.cognome, attribcibo))
    
    def dormi(self):
        print("{} {} sta dormendo".format(self.nome, self.cognome))

    def lavora(self):
        print("{} {} sta facendo il suo lavoro da {}".format(self.nome, self.cognome, self.lavoro))

def NuovaPersona(p):
    #Definisco una persona p
    nome = input("Inserisci un nome: ")
    cognome = input("Inserisci un cognome: ")
    lavoro = input("Inserisci il lavoro di {} {}: ".format(nome, cognome))
    veg = input ("{} {} è vegano? [S/N] ".format(nome, cognome))
    p.append(Persona(nome, cognome, veg, lavoro))

while True:

    if I == 0:
        NuovaPersona(p)    #La prima volta creo una persona

    while True:    #Torno a riscrivere l'elenco se scelgo di creare una nuova persona
        #Seleziono la persona
        print("Elenco persone:")
        i = 1    #Salto l'elemento 0 che è "-"
        while i < len(p):    #Un for non sono riuscito a farlo funzionare
            print(i, p[i].nome, p[i].cognome)
            i += 1

        NumeroPersona = int(input("Inserisci il numero della persona alla quale vuoi far fare qualcosa, inserisci 0 per aggiungere una nuova persona"))
        if NumeroPersona == 0:
            NuovaPersona(p)
        else:
            break
    
    #Chiedo cosa far fare a quella persona
    while True:
        Azione = input("Cosa vuoi far fare a {} {}? [Mangiare/Dormire/Lavorare] ".format(p[NumeroPersona].nome, p[NumeroPersona].cognome))
        if Azione == "M" or Azione == "D" or Azione == "L":
            break

    #Mangiare
    if Azione == "M":
        if p[NumeroPersona].veg == "S":    #Sì, la variabile Veg da stringa diventa booleana, sì in python può fare
            p[NumeroPersona].veg = True
        else:
            p[NumeroPersona].veg = False
        cibo = input("Cosa vuoi far mangiare a {} {}? ".format(p[NumeroPersona].nome, p[NumeroPersona].cognome))
        p[NumeroPersona].mangia(cibo)

    #Dormire
    elif Azione == "D":
        p[NumeroPersona].dormi()

    #Lavorare
    elif Azione == "L":
        p[NumeroPersona].lavora()

    I += 1